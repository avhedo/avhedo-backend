CREATE TABLE IF NOT EXISTS public.attachment
(
    id bigint NOT NULL,
    original_file_name character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    size bigint,
    storage_link character varying(10000) COLLATE pg_catalog."default",
    deleted boolean,
    created_date timestamp without time zone,
    type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT attachment_pkey PRIMARY KEY (id)
);
create sequence attachment_seq START 1;

CREATE TABLE IF NOT EXISTS public.tag
(
    id bigint NOT NULL,
    title character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tag_pkey PRIMARY KEY (id)
);
create sequence tag_seq START 1;

CREATE TABLE IF NOT EXISTS public.category
(
    id integer NOT NULL,
    title character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT category_pkey PRIMARY KEY (id)
);
create sequence category_seq START 1;

CREATE TABLE IF NOT EXISTS public.card
(
    id bigint NOT NULL,
    name character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    description character varying(10000) COLLATE pg_catalog."default",
    start_date timestamp without time zone,
    finish_date timestamp without time zone,
    contact_mail character varying(255) COLLATE pg_catalog."default",
    contact_phone character varying(255) COLLATE pg_catalog."default",
    category_id integer NOT NULL,
    views bigint NOT NULL DEFAULT 0,
    CONSTRAINT card_pkey PRIMARY KEY (id),
    CONSTRAINT card_category_fk FOREIGN KEY (category_id)
    REFERENCES public.category (id) MATCH SIMPLE
                         ON UPDATE NO ACTION
                         ON DELETE NO ACTION
    NOT VALID
);
create sequence card_seq START 1;

CREATE TABLE IF NOT EXISTS public.attachment_card
(
    attachment_id bigint NOT NULL,
    card_id bigint NOT NULL,
    CONSTRAINT attachment_card_pkey PRIMARY KEY (attachment_id, card_id),
    CONSTRAINT attachment_card_attachment_fk FOREIGN KEY (attachment_id)
    REFERENCES public.attachment (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
    CONSTRAINT attachment_card_card_fk FOREIGN KEY (card_id)
    REFERENCES public.card (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS public.tag_card
(
    card_id bigint NOT NULL,
    tag_id bigint NOT NULL,
    CONSTRAINT tag_card_pkey PRIMARY KEY (card_id, tag_id),
    CONSTRAINT tag_card_card_fk FOREIGN KEY (card_id)
    REFERENCES public.card (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
    CONSTRAINT tag_card_tag_fk FOREIGN KEY (tag_id)
    REFERENCES public.tag (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS public.notification
(
    id bigint NOT NULL,
    message character varying(1000) COLLATE pg_catalog."default",
    subject character varying(255) COLLATE pg_catalog."default" NOT NULL,
    created_date timestamp without time zone DEFAULT now(),
    type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT notification_pkey PRIMARY KEY (id)
);
create sequence notification_seq START 1;

CREATE TABLE IF NOT EXISTS public."user"
(
    id bigint NOT NULL,
    username character varying(16) COLLATE pg_catalog."default" NOT NULL,
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    password character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (id)
);
create sequence user_seq START 1;

CREATE TABLE IF NOT EXISTS public.notification_user
(
    notification_id bigint NOT NULL,
    user_id bigint NOT NULL,
    CONSTRAINT notification_user_pkey PRIMARY KEY (notification_id, user_id),
    CONSTRAINT notification_user_notification_fk FOREIGN KEY (notification_id)
    REFERENCES public.notification (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
    CONSTRAINT notification_user_user_id FOREIGN KEY (user_id)
    REFERENCES public."user" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS public.role
(
    id integer NOT NULL,
    title character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT role_pkey PRIMARY KEY (id)
);
create sequence role_seq START 1;

CREATE TABLE IF NOT EXISTS public.user_role
(
    user_id bigint NOT NULL,
    role_id integer NOT NULL,
    CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id),
    CONSTRAINT user_role_role_fk FOREIGN KEY (role_id)
    REFERENCES public.role (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
    CONSTRAINT user_role_user_fk FOREIGN KEY (user_id)
    REFERENCES public."user" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);