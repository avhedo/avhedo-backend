package ru.ds.avhedobackend.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class PageDto<T> {

    private List<T> content;

    private Long totalElements;

    private int totalPages;

    private int pageSize;

    private int pageNumber;

}
