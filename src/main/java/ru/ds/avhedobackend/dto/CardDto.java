package ru.ds.avhedobackend.dto;

import java.time.LocalDateTime;

public class CardDto {
    private Long id;
    private String name;
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime finishDate;
    private LocalDateTime createdDate;
    private String contactMail;
    private String contactPhone;
    private Long views;
    private CategoryDto category;
}
