package ru.ds.avhedobackend.payload;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort;

@Builder
@Getter
@Setter
public class CardSearchPayload {
    private int pageNumber;
    private int pageSize;
    private Sort.Direction sortDirection;
    private String sortProperty;
}
