package ru.ds.avhedobackend.service.card;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.ds.avhedobackend.entity.CardEntity;
import ru.ds.avhedobackend.payload.CardSearchPayload;
import ru.ds.avhedobackend.repo.CardRepo;
import ru.ds.avhedobackend.specification.Specifications;

import java.util.Collections;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {
    private final CardRepo cardRepo;

    public CardEntity save(CardEntity card) {
        return cardRepo.save(card);
    }

    @Override
    public Page<CardEntity> find(CardSearchPayload filters, int pageNumber, int pageSize, Sort.Direction sortDirection, String sortProp) {
        return cardRepo.findAll(
                Specifications.And.<CardEntity>builder()
                        .specifications(Collections.emptyList())
                        .build(),
                sortDirection == null || StringUtils.isEmpty(sortProp)
                        ? PageRequest.of(pageNumber, pageSize, Sort.Direction.ASC, "id")
                        : PageRequest.of(pageNumber, pageSize, sortDirection, sortProp));
    }
}
