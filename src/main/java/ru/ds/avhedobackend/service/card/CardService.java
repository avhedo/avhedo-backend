package ru.ds.avhedobackend.service.card;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import ru.ds.avhedobackend.entity.CardEntity;
import ru.ds.avhedobackend.payload.CardSearchPayload;

public interface CardService {
    CardEntity save(CardEntity card);

    Page<CardEntity> find(
            CardSearchPayload filters,
            int pageNumber,
            int pageSize,
            Sort.Direction sortDirection,
            String sortProp
    );
}
