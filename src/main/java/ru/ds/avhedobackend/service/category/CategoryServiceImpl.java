package ru.ds.avhedobackend.service.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.ds.avhedobackend.entity.CategoryEntity;
import ru.ds.avhedobackend.repo.CategoryRepo;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService{
    private final CategoryRepo categoryRepo;

    public CategoryEntity save(CategoryEntity category) {
        return categoryRepo.save(category);
    }
}
