package ru.ds.avhedobackend.service.category;

import ru.ds.avhedobackend.entity.CategoryEntity;

public interface CategoryService {
    CategoryEntity save(CategoryEntity category);
}
