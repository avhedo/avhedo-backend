package ru.ds.avhedobackend.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ds.avhedobackend.dto.CardDto;
import ru.ds.avhedobackend.dto.PageDto;
import ru.ds.avhedobackend.entity.CardEntity;
import ru.ds.avhedobackend.mapper.Mapper;
import ru.ds.avhedobackend.payload.CardSearchPayload;
import ru.ds.avhedobackend.service.card.CardService;

@RestController
@RequestMapping("/v1/cards")
@RequiredArgsConstructor
public class CardController {
    private final CardService cardService;
    private final Mapper mapper;

    @PostMapping
    public CardDto createCard(@RequestBody CardDto cardDto) {
        CardEntity card = mapper.map(cardDto, CardEntity.class);
        return mapper.map(cardService.save(card), CardDto.class);
    }

    @GetMapping
    public ResponseEntity<PageDto<CardDto>> find(@ModelAttribute CardSearchPayload searchPayload) {
        Page<CardEntity> page = cardService.find(searchPayload,
                searchPayload.getPageNumber(),
                searchPayload.getPageSize(),
                searchPayload.getSortDirection(),
                searchPayload.getSortProperty());

        return ResponseEntity.ok(PageDto.<CardDto>builder()
                .pageNumber(page.getNumber())
                .pageSize(page.getSize())
                .totalElements(page.getTotalElements())
                .content(mapper.mapAsList(page.getContent(), CardDto.class))
                .build());
    }
}
