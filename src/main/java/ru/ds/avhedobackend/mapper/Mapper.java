package ru.ds.avhedobackend.mapper;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import ru.ds.avhedobackend.dto.CardDto;
import ru.ds.avhedobackend.dto.CategoryDto;
import ru.ds.avhedobackend.entity.CardEntity;
import ru.ds.avhedobackend.entity.CategoryEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public class Mapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.getConverterFactory().registerConverter(new PassThroughConverter(LocalDateTime.class));
        factory.getConverterFactory().registerConverter(new PassThroughConverter(LocalDate.class));

        factory.classMap(CardEntity.class, CardDto.class)
                .field("id", "id")
                .field("name", "name")
                .field("description", "description")
                .field("startDate", "startDate")
                .field("finishDate", "finishDate")
                .field("createdDate", "createdDate")
                .field("contactMail", "contactMail")
                .field("contactPhone", "contactPhone")
                .field("views", "views")
                .field("category", "category")
                .byDefault()
                .register();

        factory.classMap(CategoryEntity.class, CategoryDto.class)
                .field("id", "id")
                .field("title", "title")
                .byDefault()
                .register();

    }
}

