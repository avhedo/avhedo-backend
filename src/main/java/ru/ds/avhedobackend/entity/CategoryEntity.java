package ru.ds.avhedobackend.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "category")
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class CategoryEntity {
    @Id
    @SequenceGenerator(name = "category_seq", sequenceName = "category_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "title")
    private String title;
}
