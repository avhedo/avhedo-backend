package ru.ds.avhedobackend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.ds.avhedobackend.entity.CardEntity;

@Repository
public interface CardRepo extends JpaRepository<CardEntity, Long>, JpaSpecificationExecutor<CardEntity> {
}
