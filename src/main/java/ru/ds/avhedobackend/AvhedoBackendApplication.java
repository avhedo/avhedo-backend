package ru.ds.avhedobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvhedoBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvhedoBackendApplication.class, args);
    }

}
